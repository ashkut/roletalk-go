//Package roletalk is asynchronous peer-to-peer microservice communication framework with its own transport protocol.
//Developed in honor of scalability, simplicity and efficiency. For more information see readme.md
package roletalk
